#!/usr/bin/python
#--------------------------------------
#    ______          __         _____            
#   /_  __/__  _____/ /_       / ___/____  __  __
#    / / / _ \/ ___/ __ \______\__ \/ __ \/ / / /
#   / / /  __/ /__/ / / /_____/__/ / /_/ / /_/ / 
#  /_/  \___/\___/_/ /_/     /____/ .___/\__, /  
#                                /_/    /____/   
#
#           validate_xml_dtd.py
#
# Demonstration of validating an XML file with a DTD schema
# that is defined within the code.
#
# Author : Matt Hawkins
# Date   : 29/01/2019
#
# https://www.tech-spy.co.uk/
#
#--------------------------------------

# Load required libraries
import os
from lxml import etree
from io import StringIO

# XML file to check
XMLFILE="validate_xml_example.xml"

# RelaxNG definition
# Root element is AddressBook and that contains one or more
# contact elements. A contact element contains firstname and
# surname and optionally, photo.
relaxngdef='''\
<element name="AddressBook" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <oneOrMore>
    <element name="contact">
      <oneOrMore>
        <element name="firstname"><text/></element>
        <element name="surname"><text/></element>
        <optional><element name="photo"><text/></element></optional>
      </oneOrMore>
    </element>
  </oneOrMore>
</element>
'''
  
# Convert defintion to file object
fr = StringIO(relaxngdef)

# Load the RelaxNG definition
relaxng_parsed = etree.parse(fr)

# Check if file exixsts
if os.path.isfile(XMLFILE):
  
  # Create RelaxNG object
  relaxng=etree.RelaxNG(relaxng_parsed)
  
  try:
    # Load XML
    tree = etree.parse(XMLFILE)
    print("  XML is valid")

    # Validate loaded XML against DTD
    if not relaxng.validate(tree):
      print("  XML does not match the schema")
    else:
      print("  XML matches the schema")

  except:
    print("  XML is not valid.")

else:
  print("XML file doesn't exist. Check XMLFILE definition.")
