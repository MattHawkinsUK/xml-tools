#!/usr/bin/python
#--------------------------------------
#    ______          __         _____            
#   /_  __/__  _____/ /_       / ___/____  __  __
#    / / / _ \/ ___/ __ \______\__ \/ __ \/ / / /
#   / / /  __/ /__/ / / /_____/__/ / /_/ / /_/ / 
#  /_/  \___/\___/_/ /_/     /____/ .___/\__, /  
#                                /_/    /____/   
#
#           validate_xml_relaxng.py
#
# Demonstration of validating an XML file with a RelaxNG schema
# that is defined within the code.
#
# Author : Matt Hawkins
# Date   : 29/01/2019
#
# https://www.tech-spy.co.uk/
#
#--------------------------------------

# Load required libraries
import os
from lxml import etree
from io import StringIO

# XML file to check
XMLFILE="validate_xml_example.xml"

# DTD definition
# Root element is AddressBook and that contains one or more
# contact elements. A contact element contains firstname and
# surname and optionally, photo. Elements must appear in the order
# specified.
DTDDEF='''<!ELEMENT AddressBook (contact+)>
<!ELEMENT contact (firstname,surname,photo?)>
<!ELEMENT firstname (#PCDATA)>
<!ELEMENT surname (#PCDATA)>
<!ELEMENT photo (#PCDATA)>
'''
# Convert defintion to file object
fd = StringIO(DTDDEF)

# Check if file exixsts
if os.path.isfile(XMLFILE):
  
  # Create DTD object
  dtd = etree.DTD(fd)
  
  try:
    # Load XML
    tree = etree.parse(XMLFILE)
    print("  XML is valid")

    # Validate loaded XML against DTD
    if not dtd.validate(tree):
      print("  XML does not match the DTD schema")
    else:
      print("  XML matches the DTD schema")

  except:
    print("  XML is not valid.")

else:
  print("XML file doesn't exist. Check XMLFILE definition.")
